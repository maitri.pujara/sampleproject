package com.camunda.invoice.main;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.camunda.invoice.client.InvoiceBpmnClient;

@SpringBootApplication
@PropertySource(value = { "classpath:application.properties" })
@ComponentScan(basePackages = { "com.camunda.invoice" })
public class CamundaApplication {
	
	/*
	 * @Value("${invoice.xml.remote.url}") private static String url;
	 */
	
	public static void main(String[] args) {
		SpringApplication.run(CamundaApplication.class, args);
		//System.out.println("+++++++++++++"+url);
		InvoiceBpmnClient client=new InvoiceBpmnClient();
		client.retieveXmlFromRemoteLocation();

}
	
	 @Bean
	 public RestTemplate restTemplate(RestTemplateBuilder builder) {
	        return builder.build();
	  }
}
