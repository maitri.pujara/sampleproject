package com.camunda.invoice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Invoice implements Serializable{

	private String id;
	private String bpmn20Xml;
	
}
