package com.camunda.invoice.client;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.Stack;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.camunda.invoice.model.Invoice;
import com.camunda.invoice.model.ProcessGraph;

@Component
public class InvoiceBpmnClient {
	
    private static final String START = "StartEvent_1";
    private static final String END = "invoiceNotProcessed";

    
	//@Autowired
	private RestTemplate restTemplate;
	
//	@Value("${invoice.xml.remote.url}")
	private String uri="https://n35ro2ic4d.execute-api.eu-central-1.amazonaws.com/prod/engine-rest/process-definition/key/invoice/xml";
	
	Environment env;
	
	public void retieveXmlFromRemoteLocation(){
		//RestTemplate
		restTemplate=new RestTemplate();
		
		 try {
			 URI url=new URI(this.uri);
			// System.out.println("++++++++++++++"+this.uri+"URI"+uri);
			 ResponseEntity<Invoice> response=restTemplate.getForEntity(url, Invoice.class);
//			 if(HttpStatus.OK == response.getStatusCode()){
//				 System.out.println(response.getBody().getBpmn20Xml());
//				}
			 
			 InputStream targetStream = new ByteArrayInputStream(response.getBody().getBpmn20Xml().getBytes());
			 BpmnModelInstance modelInstance = Bpmn.readModelFromStream(targetStream);
			 
			 FlowNode flowNode = (FlowNode) modelInstance.getModelElementById(START);
			 
			 ProcessGraph processGraph = new ProcessGraph();
			 
		    Set<FlowNode> visited = new LinkedHashSet<FlowNode>();
		    Stack<FlowNode> stack = new Stack<FlowNode>();
		    stack.push(flowNode);
		    while (!stack.isEmpty()) {
		    	FlowNode vertex = stack.pop();
		        if (!visited.contains(vertex)) {
		            visited.add(vertex);
		            Collection<SequenceFlow> c = vertex.getOutgoing();
		            for (SequenceFlow sequenceFlow : c) {
		            	FlowNode f = sequenceFlow.getTarget();
		            	processGraph.addEdge(vertex.getId(), f.getId());
		            	stack.push(f);
					}
		        }
		    }
		    
		    
		       LinkedList<String> result = new LinkedList();
		       result.add(START);
		       depthFirst(processGraph, result);
			 
			} catch (Exception e) {
				e.printStackTrace();
			
			}
	
	}
	
    private void depthFirst(ProcessGraph graph, LinkedList<String> visited) {
        LinkedList<String> nodes = graph.adjacentNodes(visited.getLast());
        // examine adjacent nodes
        for (String node : nodes) {
            if (visited.contains(node)) {
                continue;
            }
            if (node.equals(END)) {
                visited.add(node);
                printPath(visited);
                visited.removeLast();
                break;
            }
        }
        for (String node : nodes) {
            if (visited.contains(node) || node.equals(END)) {
                continue;
            }
            visited.addLast(node);
            depthFirst(graph, visited);
            visited.removeLast();
        }
    }

    private void printPath(LinkedList<String> visited) {
//        for (String node : visited) {
//            System.out.print(node);
//            System.out.print(" ");
//        }
      System.out.print(" ");
      System.out.print(" ");

        System.out.println("The path from approveInvoice to invoiceProcessed is: " + visited);
    }
}
